# AZ Nutrição & Vitaminas

React front-end project of an ecommerce to AZ Nutrilçao & Vitaminas company.

:robot: Project in development... :robot:

Development project of an e-commerce system for the company AZ Nutrição, using the Tray Commerce platform and React. Thus, the technologies used are:

- PHP
- Twig
- jQuery
- React
- Tray REST API

## Project access url
[www.aznutricao.com.br](https://www.aznutricao.com.br)

![](images/home.webp)

## Features
- [x] = Implemented
- [ ] = Not implemented yet

### Functional requirements
#### User settings
- [x] Login
- [ ] Register

#### Product cart
- [x] Add product to cart, including product variations options
- [x] Add multiple products to cart at once, including product variations options
- [x] Increase the quantity of the product in the cart, directly from cart
- [x] Remove product from cart
- [x] Swipe to remove product from cart on mobile

#### Header
- [x] Header must include product categogries, subcategories, brands, searchbar and a buttons to Login - Register / Access account - Logout
- [ ] Each product category on the header must contain a featured product (product image + add to cart button)
- [x] Header must minimize its own size and adapt to take up less screen space on page scroll

#### Home page
- [x] Must iclude a hero with some products / categories infos
- [x] Must include some banners with some products / categories infos
- [x] Must include some showcases with products


### Non-functional requirements
- [x] Project must use React components
- [x] Project must follow the practices of the Tray Commerce platform documentation, available at - [Tray Documentation](https://atendimento.tray.com.br/hc/pt-br/categories/360002365692-Opencode)
- [ ] All interface components must be responsive
- [ ] The project must be developed following the Mobile First rules
- [ ] All images used in the project must be optimized in size, weight and format for mobile devices and networks
- [ ] All CSS and JS files must be minified
- [ ] The project must achieve at least 80 performance points in Google Audit and follow Google Development Rules for Performance and SEO

© 2021 Alisson Cipriano. All rights reserved.

